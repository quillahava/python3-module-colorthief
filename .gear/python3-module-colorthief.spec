%define _unpackaged_files_terminate_build 1
%define pypi_name colorthief

Name: python3-module-%pypi_name
Version: 0.2.1
Release: alt1
Summary: Python library for extracting colors from images
License: BSD
Group: Development/Python3
Url: https://pypi.org/project/colorthief
Vcs: https://github.com/fengsp/color-thief-py
BuildArch: noarch
Source0: %name-%version.tar

BuildRequires(pre): rpm-build-python3
BuildRequires: python3(setuptools)
BuildRequires: python3(wheel)

%description
Python3-colorthief is a library that helps you extract colors from images.

%prep
%setup

%build
%pyproject_build

%install
%pyproject_install

%files
%doc README.rst CHANGES LICENSE
%python3_sitelibdir/%pypi_name.py
%python3_sitelibdir/__pycache__/
%python3_sitelibdir/%{pyproject_distinfo %pypi_name}/

%changelog
* Tue Jun 25 2024 Aleksandr A. Voyt <sobue@altlinux.org> 0.2.1-alt1
- Initial commit
